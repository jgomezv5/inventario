<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Prueba Julián PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    </head>
    <body>

        <div class="w3-container">
            
            <image class="w3-margin" src="<?php echo base_url("assets/img/logo-konecta-pie.svg") ?>">

            <div class="w3-bar">
                <a href="<?php echo base_url("index.php/ventas") ?>" class="w3-bar-item w3-button w3-black" style="width:50%">Ventas</a>
                <a href="<?php echo base_url("index.php/productos") ?>"class="w3-bar-item w3-button w3-teal" style="width:50%">Productos</a>
            </div>
            
            <?php if($cod == 2) { ?>
            
            <div class="w3-panel w3-yellow w3-display-container w3-card-4">
                <span onclick="this.parentElement.style.display='none'"
                      class="w3-button w3-large w3-display-topright">&times;</span>
                <h3>Atención!</h3>
                <p><?php echo $mensaje ?></p>
            </div>
            
            <?php } ?>
            
            <?php if($cod == 1) { ?>
            
            <div class="w3-panel w3-green w3-display-container w3-card-4">
                <span onclick="this.parentElement.style.display='none'"
                      class="w3-button w3-large w3-display-topright">&times;</span>
                <h3>Correcto!</h3>
                <p><?php echo $mensaje ?></p>
            </div>
            
            <?php } ?>
            
            <div id="Productos" class="w3-container w3-border pestana">
                <h2>Vender producto</h2>
                
                <br/>
                <br/>
                
                <div class="w3-cell-row">
                    <div class="w3-container w3-cell w3-cell-top"></div>
                    
                    <div class="w3-container w3-cell w3-cell-middle">
                        
                        <form class="w3-container w3-card-4" method="post" action="<?php echo base_url("index.php/ventas/vender") ?>">
                            <p>
                                <label>Id Producto</label>
                                <input name="id_producto" class="w3-input" type="text" required></p>
                            
                            <p>
                            <button class="w3-btn w3-teal">Vender</button></p>
                            
                        </form>
                        
                    </div>
                    <div class="w3-container w3-cell w3-cell-bottom"></div>
                </div>
                
                <br/>
                
            </div>

        </div>
        

    </body>
</html>