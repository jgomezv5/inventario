<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Prueba Julián PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    </head>
    <body>

        <div class="w3-container">
            
            <image class="w3-margin" src="<?php echo base_url("assets/img/logo-konecta-pie.svg") ?>">

            <div class="w3-bar">
                <a href="<?php echo base_url("index.php/ventas") ?>" class="w3-bar-item w3-button w3-black" style="width:50%">Ventas</a>
                <a href="<?php echo base_url("index.php/productos") ?>"class="w3-bar-item w3-button w3-teal" style="width:50%">Productos</a>
            </div>
            
            <div id="Productos" class="w3-container w3-border pestana">
                <h2>Administrar Productos</h2>
                
                <a href="<?php echo base_url("index.php/productos/nuevo/") ?>" class="w3-button w3-xlarge w3-circle w3-teal">+</a> Nuevo
                
                <br/>
                <br/>
                
                <table class="w3-table-all w3-hoverable">
                    <thead>
                        <tr class="w3-light-grey">
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Referencia</th>
                            <th>Precio</th>
                            <th>Peso</th>
                            <th>Categoría</th>
                            <th>Stoc</th>
                            <th>F. Creación</th>
                            <th>F. Ult. Venta</th>
                            <th>Acción</th>
                            
                        </tr>
                    </thead>
                    
                    <tbody>
                        
                        <?php
                        
                        if (sizeof($datos) !== 0) {
                            
                            foreach ($datos as $fila) {
                                
                                echo "<tr>";
                                
                                foreach ($fila as $key => $value) {
                                    echo "<td>" . $value . "</td>";
                                }
                                
                                echo "<td>"
                                . "<a _target=\"blank\" href=\"" . base_url("index.php/productos/editar/" . $fila["id_producto"]) ."\" class=\"w3-button w3-black\"><i class='fas fa-edit'></i></a>"
                                . "<a _target=\"blank\" onclick=\"borrar(" . $fila["id_producto"] . ")\" class=\"w3-button w3-black\"><i class='fas fa-trash'></i></a>"
                                . "</td>"
                                . "</tr>";
                                
                            }
                            
                        }
                        
                        ?>
                        
                        
                    </tbody>
                    
                </table>
                
            </div>

        </div>
        

        <script>
            
            function borrar(that){
                
                var res = confirm("Borrar " + that + "?" );
                
                
                if(res){
                    
                    location.href = "<?php echo base_url("index.php/productos/borrar/") ?>" + that;
                    
                }
                
            }

            
        </script>

    </body>
</html>