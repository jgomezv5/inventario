<?php
defined('BASEPATH') OR exit('No direct script access allowed');

switch ($datos) {
    case "nuevo":
        $url_action = base_url("index.php/productos/nuevo/add");
        break;
    
    case "editar":
        $url_action = base_url("index.php/productos/editar_ok");
        break;

    default:
        //$url_action = base_url("index.php/productos/nuevo/add");
        break;
}

?><!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Prueba Julián PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    </head>
    <body>

        <div class="w3-container">
            
            <image class="w3-margin" src="<?php echo base_url("assets/img/logo-konecta-pie.svg") ?>">
            
            <br/>
            
            <div class="w3-bar">
                <a href="<?php echo base_url("index.php/productos") ?>" class="w3-bar-item w3-button w3-black" style="width:50%">Volver</a>
            </div>
            
            <br/>
            
            <header class="w3-container w3-teal"> 

                <h2>Detalle Producto (<?php echo $datos ?>)</h2>
                
            </header>
            <div class="w3-container">

                <br/>
                
                <form class="w3-container w3-card-4" action="<?php echo $url_action ?>" method="post">

                    <?php if (strcmp($datos, "nuevo") != 0) { ?>
                        <p>
                            <label><?php echo (strcmp($datos, "editar") == 0) ? "" : "Id" ?></label>
                            <input name="id_producto" class="w3-input" type="<?php echo (strcmp($datos, "editar") == 0) ? "hidden" : "number" ?>" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["id_producto"] : "" ?>" ></p>
                    <?php } ?>
                    
                    <p>
                        <label>Nombre</label>
                        <input name="nombre" class="w3-input" type="text" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["nombre"] : "" ?>"></p>
                    <p>
                        <label>Referencia</label>
                        <input name="referencia" class="w3-input" type="text" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["referencia"] : "" ?>"></p>

                    <p>
                        <label>Precio</label>
                        <input name="precio" class="w3-input" type="number" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["precio"] : "" ?>"></p>

                    <p>
                        <label>Peso</label>
                        <input name="peso" class="w3-input" type="number" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["peso"] : "" ?>"></p>

                    <p>
                        <label>Categoria</label>
                        <input name="categoria" class="w3-input" type="text" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["categoria"] : "" ?>"></p>

                    <p>
                        <label>Stock</label>
                        <input name="stock" class="w3-input" type="number" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["stock"] : "" ?>"></p>

                    <p>
                        <label>F. Creación</label>
                        <input name="f_creacion" class="w3-input" type="date" required value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["f_creacion"] : "" ?>"></p>

                    <?php if (strcmp($datos, "nuevo") != 0) { ?>
                        <p>
                            <label>F. Ult. Venta</label>
                            <input name="f_ultima_venta" class="w3-input" type="datetime-local" value="<?php echo (strcmp($datos, "editar") == 0) ? $resultado[0]["f_ultima_venta"] : "" ?>"></p>
                    <?php } ?>
                    
                    <p>
                    <button class="w3-btn w3-teal">Aceptar</button></p>

                </form>
                
            </div>
            
            <br/>
            
            <footer class="w3-container w3-teal">
                <p>Julian Gomez - Octubre 2020</p>
            </footer>

        </div>

    </body>
    
</html>