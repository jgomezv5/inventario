<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Producto extends CI_Model {

    var $table = "productos";
    
    function __construct() {
        parent::__construct();
    }
    
    function construct(){}
    
    // Obtener el listado de productos general
    function get_productos() {

        $sql = "SELECT * FROM productos ORDER BY nombre ASC";        
        $query = $this->db->query($sql);
        return $query->result_array();
        
    }
    
    //Función para seleccionar un producto
    public function get_producto($id) {
        $query = $this->db->get_where($this->table,$id);
        return $query->result_array();
    }

    
    function add_producto($data) {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    //Función para actualizar producto
    public function update_producto($id,$data){
        $this->db->set($data);
        $this->db->where('id_producto',$id);
        $this->db->update($this->table,$data);
    }

    //Funcion para borrar producto
    public function delete_producto($id){
        $data = array(
            'id_producto' => $id            
        );
        $this->db->delete($this->table,$data);
    }

    
}
