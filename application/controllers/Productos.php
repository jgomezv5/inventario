<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    
    public function index() {

        $this->load->helper('url');
        
        $this->load->model("producto", "modelo");

        $resultado['datos'] = $this->modelo->get_productos();

        $this->load->view('productos', $resultado);
    }
    
    public function nuevo($param = 0) {
        
        $this->load->helper('url');
        
        if(strcasecmp($param, "add") == 0){
            
            $dat = ($_POST);
            
            $this->load->model("producto", "modelo");
            $resultado = $this->modelo->add_producto($dat);
            
            header("Location: " . base_url("index.php/productos"));
            
        }else{
            $inf["datos"] = "nuevo";
            $this->load->view('formulario', $inf);
        }
        
    }
    
    public function editar($param = 0) {
        
        $this->load->helper('url');

        $this->load->model("producto", "modelo");

        $inf["datos"] = "editar";
        
        $inf["resultado"] = $this->modelo->get_producto(array("id_producto" => $param));;
        
        $this->load->view('formulario', $inf);
        
        
    }
    
    public function editar_ok() {
        
        $this->load->helper('url');
        
        $data = $_POST;
        
        $id = $data["id_producto"];
        
        unset($data["id_producto"]);
        
        $this->load->model("producto", "modelo");
        $resultado = $this->modelo->update_producto($id, $data);
            
        header("Location: " . base_url("index.php/productos"));
        
        
    }
    
    public function borrar($param = 0) {
        
        $this->load->helper('url');
        
        $this->load->model("producto", "modelo");
        $resultado = $this->modelo->delete_producto($param);
        
        header("Location: " . base_url("index.php/productos"));
        
        
    }
    
}
