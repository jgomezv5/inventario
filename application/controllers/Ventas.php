<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    
    public function index() {

        $inf["cod"] = 0;
        
        $this->load->helper('url');
        
        $this->load->view('ventas',$inf);
        
    }
    
    public function vender() {
        
        $this->load->helper('url');
        
        $this->load->model("producto", "modelo");
        $resultado = $this->modelo->get_producto(array("id_producto" => $_POST["id_producto"]));
        
        if(isset($resultado[0]["stock"])){
        
            if($resultado[0]["stock"] > 0){

                $data = $resultado[0];

                unset($data["id_producto"]);

                $data["stock"] = $data["stock"] - 1;

                $data["f_ultima_venta"] = date("Y-m-d h:m:s");

                $this->modelo->update_producto($_POST["id_producto"],$data);

                $inf["cod"] = 1;

                $inf["mensaje"] = "Venta del producto (" . $_POST["id_producto"] . ") realizada correctamente.";

                $this->load->view('ventas', $inf);

            }else{

                $inf["cod"] = 2;

                $inf["mensaje"] = "Producto (" . $_POST["id_producto"] . ") sin existencias.";

                $this->load->view('ventas', $inf);

            }
        }else{
            
            $inf["cod"] = 0;
        
            $this->load->helper('url');

            $this->load->view('ventas',$inf);
            
        }
        
    }
    
}
