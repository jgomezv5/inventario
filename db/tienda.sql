-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-10-2020 a las 22:54:08
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `referencia` varchar(255) NOT NULL,
  `precio` int(11) NOT NULL,
  `peso` int(11) NOT NULL,
  `categoria` varchar(30) NOT NULL,
  `stock` int(11) NOT NULL,
  `f_creacion` date NOT NULL,
  `f_ultima_venta` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `referencia`, `precio`, `peso`, `categoria`, `stock`, `f_creacion`, `f_ultima_venta`) VALUES
(1, 'Papas', '0001', 1200, 1, 'Alimentos', 7, '2020-10-31', '2020-10-31 10:10:46'),
(2, 'Horno microondas', '0002', 150000, 10, 'Electrofomesticos', 5, '2020-10-31', '2020-10-31 10:00:00'),
(5, 'zzz', 'zzz', 12312, 12312, '312', 12312, '2020-10-31', '0000-00-00 00:00:00'),
(7, 'Nevera', 'Nev03', 250, 10, 'Electrodomestico', 222, '2020-10-31', '0000-00-00 00:00:00'),
(8, '666', '666', 66, 666, '66', 1, '2020-10-31', '0000-00-00 00:00:00'),
(9, '777', '77', 777, 777, '777', 0, '2020-10-31', '2020-10-31 10:10:35');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `categoria` (`categoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
