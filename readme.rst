Demo: https://servicomgv.com/inventario/

###################
Sistema de inventario
###################

Sistema diseñado para la prueba solicitada en PHP, para optimizar tiempos se hará uso del framework CodeIgniter que no es intrusivo ni requiere de ningún orquestador de paquetes.
la plataforma base es PHP 5.6.12 y Mysql 5.6.26 (Xampp 6.5.12)


Versión de CodeIgniter: 3 (Link: https://api.github.com/repos/bcit-ci/CodeIgniter/zipball/3.1.11)
CSS W3css: https://www.w3schools.com/w3css/defaulT.asp

XAMPP 5.6.12 (Windows): https://sourceforge.net/projects/xampp/files/XAMPP%20Windows/5.6.12/xampp-win32-5.6.12-0-VC11-installer.exe/download
XAMPP 5.6.12 (Linux): https://sourceforge.net/projects/xampp/files/XAMPP%20Linux/5.6.12/xampp-linux-x64-5.6.12-0-installer.run/download


*******************
Detalles de código
*******************

Controladores:

application -> controllers

Modelos:

application -> models

Vistas:

application -> views


Assets (imagenes):

/assets


Archivos manipulados:

application/config/routes.php (Establece el controlador por defecto que se usará)
application/config/database.php (Establece el controlador por defecto que se usará)
application/config/autoload.php (Establece el controlador por defecto que se usará)
application/controllers/Inicio.php (Lógica del controlador principal)
application/views/inicio.php (Vistas de cara al cliente/usuario)
application/views/productos.php
application/views/formulario.php
application/views/ventas.php
application/models/producto.php

*******************
Detalles técnicos
*******************

Base de datos: tienda (Se encuentra en la carpeta db para importar a MySql)
Codificación: utf8_general_ci

Tabla: productos
campos: 9
